def actual(m):
    if m.find('pinned')>=0:
        return True
    if m.find('removed')>=0:
        return True
    if m.find('changed group photo')>=0:
        return True
    if m.find('invited')>=0:
        return True
    return False

def serv(s):
    if actual(s):
        return cropString(s)

ms = list(filter(actual, map(lambda x: cropString(x.text), mservice)))
ms.sort()

# пины
pin = [re.search("\w+ \w+",m)[0] for m in ms if 'pinned' in m]

cc_auth = Counter(pin).most_common()
for c in cc_auth[:]:
    print(c)

# смена картинки
changed = [re.search("\w+ \w+",m)[0] for m in ms if 'changed group photo' in m]

cc_auth = Counter(changed).most_common()
for c in cc_auth[:]:
    print(c)

# удалился
removed = [re.search("\w+ \w+",m)[0] for m in ms if 'removed' in m]

cc_auth = Counter(removed).most_common()
for c in cc_auth[:]:
    print(c)

# добавил
invited = [re.search("\w+ \w+",m)[0] for m in ms if 'invited' in m]

cc_auth = Counter(invited).most_common()
for c in cc_auth[:]:
    print(c)
