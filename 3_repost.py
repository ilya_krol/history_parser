reps = [m for m in msgs if 'repost' in m]

cc_reps = Counter([r['author'] for r in reps]).most_common()
for a,c in cc_reps:
    print(a,c,sep='|')

cc_src = Counter([r['src'] for r in reps]).most_common()
for s,c in cc_src[:10]:
    print(s,c,sep='|')


for auth,_ in cc_reps:
    print(auth)
    auth_reps = [m for m in reps if m['author']==auth]
    cc = Counter([r['src'] for r in auth_reps]).most_common()
    for s,c in cc[:5]:
        print(s,c,sep='|')
    print()



def getSite(s):
    ss = re.search('([\w\-\_]+\.\w+)(/|$)', s)
    if ss is not None:
        return ss[1].lower()

#list(map(getSite, sites))


def getExt(s):
    ss = re.search('.+/.+\.(\w+)$', s)
    if ss is not None:
        return ss[1].lower()

# ссылки
linked = [m for m in msgs if 'links' in m]

links = reduce(operator.concat, map(lambda m: [(m['author'], l, getSite(l), getExt(l)) for l in m['links']], linked))


# самые популярные домены
cc_site = Counter([s for (a, l, s, e) in links]).most_common()
for a,b in cc_site[:20]:
    print(a,b,sep='|')


# самые популярные авторы ссылок
cc_author = Counter([a for (a, l, s, e) in links]).most_common()
for a,b in cc_author:
    print(a,b,sep='|')


# самые популярные домены (автор)
for auth,_ in cc_author:
    print(auth)
    cc = Counter([s for (a, l, s, e) in links if a==auth]).most_common()
    for a,b in cc[:]:
        print(a,b,sep='|')
    print()


# самые популярные типы ссылок (автор)
for auth,_ in cc_author:
    print(auth)
    cc = Counter([e for (a, l, s, e) in links if a==auth]).most_common()
    for c in cc[:7]:
        print(c)
    print()


# хештеги
ht = [m for m in msgs if 'hashtags' in m]

hts = reduce(operator.concat, map(lambda m: [(m['author'], h) for h in m['hashtags']], ht))


# самые популярные хештеги
cc_hts = Counter([h for (_, h) in hts]).most_common()
for a,b in cc_hts[:]:
    print(a,b,sep='|')

# самые популярные авторы хештегов
cc_author = Counter([a for (a, _) in hts]).most_common()
for a,b in cc_author:
    print(a,b,sep='|')

# самые популярные хештеги (автор)
for auth,_ in cc_author:
    print(auth)
    cc = Counter([h for (a, h) in hts if a==auth]).most_common()
    for a,b in cc[:7]:
        print(a,b,sep='|')
    print()







# вызовы
call = [m for m in msgs if 'calls' in m]

calls = reduce(operator.concat, map(lambda m: [(m['author'], h) for h in m['calls']], call))


# самые популярные вызовы
cc_calls = Counter([h for (_, h) in calls]).most_common()
for c in cc_calls[:]:
    print(c)

# самые популярные авторы вызовов
cc_author = Counter([a for (a, _) in calls]).most_common()
#for c in cc_author:
#    print(c)

# самые популярные вызовы (автор)
for auth,_ in cc_author:
    print(auth)
    cc = Counter([h for (a, h) in calls if a==auth]).most_common()
    for c in cc[:]:
        print(c)
    print()
