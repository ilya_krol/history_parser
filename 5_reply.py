reply = [m for m in msgs if 'replyTo' in m]

#cc_auth = Counter([r['author'] for r in reply]).most_common()
#for c in cc_auth:
#    print(c)


#cc_src = Counter([r['replyTo'] for r in reply]).most_common()
#for c in cc_src[:20]:
#    print(c)



reply_index = {m['id']:m for m in msgs}

def getRepAuth(m):
    if m['replyTo'] in reply_index:
        return reply_index[m['replyTo']]['author']
    else:
        return None

reps = list(map(lambda m: (m['author'], getRepAuth(m)), reply))

# самые популярные авторы реплаев
cc_author = Counter([a for (a, _) in reps]).most_common()
for a,b in cc_author:
    print(a,b,sep='|')


# самые популярные кому отвечают
cc_reps = Counter([h for (_, h) in reps]).most_common()
for a,b in cc_reps[:]:
    print(a,b,sep='|')

# самые популярные кому отвечают (автор)
for auth,_ in cc_author:
    print(auth)
    cc = Counter([h for (a, h) in reps if a==auth]).most_common()
    for c in cc[:]:
        print(c)
    print()




def getReplyChain(m):
    if 'replyTo' not in m:
        print(m['author'])
        print(m['text'])
        print()
    else:
        print(m['author'])
        print(m['text'])
        print()
        getReplyChain(reply_index[m['replyTo']])



def getReplyDepth(m):
    if 'replyTo' not in m:
        return 0
    else:
        id = m['replyTo']
        if id not in reply_index:
            print('not found reply:', id)
            return 0
        else:
            return getReplyDepth(reply_index[id]) + 1


max = 0
saved = None
for m in msgs:
    d = getReplyDepth(m)
    if d > max:
        max = d
        saved = m


