
# средняя длина сообщения

texted = [m for m in msgs if 'text' in m]

auth_texted = {}
for auth,_ in cc_auth:
    auth_texted[auth] = [m for m in texted if m['author']==auth]

cnt = 0
tlen = 0

for t in texted:
    cnt = cnt + 1
    tlen = tlen + len(t['text'])

print(tlen/cnt)

# средняя длина сообщения (автор)
for auth,_ in cc_auth:
    print(auth)
    cnt = 0
    tlen = 0
    for t in auth_texted[auth]:
        cnt = cnt + 1
        tlen = tlen + len(t['text'])
    print(tlen/cnt)
    print()

# частые слова
words = list(filter(None, reduce(operator.concat, map(lambda x: re.split('\W|\s\d', x['text']), texted))))

cc_words = Counter([w.lower() for w in words]).most_common()
for c in cc_words[:100]:
    print(c)


# частые слова (автор)
for auth,_ in cc_auth:
    print(auth)
    auth_words = list(filter(None, reduce(operator.concat, map(lambda x: re.split('\W|\s', x['text']), auth_texted[auth]))))
    cc_words = Counter([w.lower() for w in auth_words]).most_common()
    for c in cc_words[:20]:
        print(c)
    print()

