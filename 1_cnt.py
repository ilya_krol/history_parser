#количество сообщений 
len(msgs)

# по пользователям
cc_auth = Counter([r['author'] for r in msgs]).most_common()
for a, b in cc_auth:
    print(a, b, sep='|')

auth_msgs = {}
for auth,_ in cc_auth:
    auth_msgs[auth] = [m for m in msgs if m['author']==auth]

# по месяцам
cc_month = Counter([r['dt'].month for r in msgs]).most_common()
for c in cc_month:
    print(c)

# по месяцам (автор)
for auth,_ in cc_auth:
    print(auth)
    cc = Counter([r['dt'].month for r in auth_msgs[auth]]).most_common()
    for c in cc[:]:
        print(c)
    print()


# по дням недели
cc_wd = Counter([r['dt'].isoweekday() for r in msgs]).most_common()
for c in cc_wd:
    print(c)

# по дням недели (автор)
for auth,_ in cc_auth:
    print(auth)
    cc = Counter([r['dt'].isoweekday() for r in auth_msgs[auth]]).most_common()
    for c in cc[:]:
        print(c)
    print()


def getDtStr(dt):
    return dt.strftime('%d.%m.%Y')

# по дням
cc_wd = Counter([getDtStr(r['dt']) for r in msgs]).most_common()
for c in cc_wd[:20]:
    print(c)

# по дням (автор)
for auth,_ in cc_auth:
    print(auth)
    cc = Counter([getDtStr(r['dt']) for r in auth_msgs[auth]]).most_common()
    for c in cc[:5]:
        print(c)
    print()


# по часам
cc_wd = Counter([r['dt'].hour for r in msgs]).most_common()
for a, b in cc_wd[:]:
    print(a, b, sep='|')

# по часам в рабочий день
cc_wd = Counter([r['dt'].hour for r in filter(lambda x: isWorkDay(x['dt']), msgs)]).most_common()
for a, b in cc_wd[:]:
    print(a, b, sep='|')

# по часам в выходной день
cc_wd = Counter([r['dt'].hour for r in filter(lambda x: not isWorkDay(x['dt']), msgs)]).most_common()
for a, b in cc_wd[:]:
    print(a, b, sep='|')


auth = 'Сергей Котов'

def print_auth_hour(auth):
    # по часам(автор)
    cc_wd1 = Counter([r['dt'].hour for r in auth_msgs[auth]])
    # по часам в рабочий день(автор)
    cc_wd2 = Counter([r['dt'].hour for r in filter(lambda x: isWorkDay(x['dt']), auth_msgs[auth])])
    # по часам в выходной день(автор)
    cc_wd3 = Counter([r['dt'].hour for r in filter(lambda x: not isWorkDay(x['dt']), auth_msgs[auth])])
    for i in range(1, 24):
        print(i, cc_wd1[i], round(cc_wd1[i]/365,2), cc_wd2[i], round(cc_wd2[i]/247,2), cc_wd3[i], round(cc_wd3[i]/118,2), sep='|')
    i = 0    
    print(24, cc_wd1[i], round(cc_wd1[i]/365,2), cc_wd2[i], round(cc_wd2[i]/247,2), cc_wd3[i], round(cc_wd3[i]/118,2), sep='|')







# по дням (пустые дни, выхи)
cc_wd = Counter([r['dt'].date() for r in msgs])
for c in days2019():
    if cc_wd[c] == 0 and not isWorkDay(c):
        print(getDtStr(c), cc_wd[c], sep='|')
