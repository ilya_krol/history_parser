from lxml import html
from glob import glob
from os import chdir
import re
import datetime
from collections import Counter
import operator
from functools import reduce



def getFileNum(f):
    num = re.search('\d+', f)
    if (num):
        return int(num[0])
    else:
        return 1

chdir(r'D:\test\hist\messages')
files = glob('messages*.html')
files.sort(key=lambda x: getFileNum(x))


messages = []
mservice = []

for filename in files:
    with open(filename, encoding="utf-8") as f:
        page = f.read()
        tree = html.fromstring(page)
        messages_ = tree.xpath('//div[@class="history"]/div[contains(@class, "message default")]/div[@class="body"]')
        messages = messages + messages_
        mservice_ = tree.xpath('//div[@class="history"]/div[contains(@class, "message service")]/div[@class="body details"]')
        mservice = mservice + mservice_

def cropString(s):
    if re.search('\n\s*$', s):
        return cropString1(s)
    else:
        return cropString2(s)

def cropString1(s):
    b = s.find('\n')
    e = s.rfind('\n')
    return s[b+1:e]

def cropString2(s):
    b = s.find('\n')
    return s[b+1:]

def getFromName(s):
    ss = re.search('^\w+ \w+', s)
    if (ss is not None):
        return ss[0]
    else:
        return re.search('^\w+', s)[0]

rawAuthor = ''

def getRawAuthor(m):
    global rawAuthor
    fr = m.find('div[@class="from_name"]')
    if (fr is not None):
        rawAuthor = cropString(fr.text_content())
    return rawAuthor

def getAuthor(m):
    return getFromName(getRawAuthor(m))

def getDt(m):
    dt = m.xpath('div[contains(@class, "date")]/@title')
    return datetime.datetime.strptime(dt[0], '%d.%m.%Y %H:%M:%S')

def getText(m):
    textDiv = m.find('div[@class="text"]')
    if (textDiv is not None):
        return cropString(' '.join(textDiv.xpath("text()")))

def getLinks(m):
    textDiv = m.find('div[@class="text"]')
    if (textDiv is not None):
        linkTexts = textDiv.xpath('a/text()')
        linkHrefs = textDiv.xpath('a/@href')
        if linkTexts:
            hashtags = []
            calls = []
            links = []
            for l in linkTexts:
                if re.search('^\#', l):
                    hashtags.append(l)
                elif re.search('^\@', l):
                    calls.append(l)
                else:
                    if not linkHrefs[linkTexts.index(l)]:
                        calls.append(l)
                    else:
                        hr = linkHrefs[linkTexts.index(l)]
                        if (hr.find('mailto:')==-1 and hr.find('tel:')==-1):
                            links.append(linkHrefs[linkTexts.index(l)])
            return (links, calls, hashtags)

def getVia(m):
    rawAuthor = getRawAuthor(m)
    if rawAuthor.find('via') >= 0:
        return (True, re.search('via @\w+', rawAuthor)[0])
    else:
        return (False, '')

def getForvarded(m):
    forvardedDiv = m.find('div[@class="forwarded body"]')
    if (forvardedDiv is not None):
        fr = forvardedDiv.find('div[@class="from_name"]')
        if (fr is not None):
            return (True, cropString(fr.text))
        else:
            return (True, '')
    else:
        return (False, '')

def getContent(m):
    div = m.find('div/div/div/div[@class="title bold"]')
    if (div is not None):
        return cropString(div.text_content())
    else:
        return None

def getReplyTo(m):
    div = m.find('div[@class="reply_to details"]')
    if (div is not None):
        href = div.find('a').get('href')
        res = re.search('go_to_message((?:\d+))', href)[1]
        return (True, int(res))
    else:
        return (False, 0)

def getId(m):
    return int(re.search('\d+', m.getparent().get('id'))[0])

msgs = []

for m in messages:
    msg = {}
    msg['id'] = getId(m)
    msg['author'] = getAuthor(m)
    msg['dt'] = getDt(m)
#    print(getDt(m))
    
    text = getText(m)
    if (text is not None):
        msg['text'] = text
    
    via = getVia(m)
    if (via[0] and 'text' not in msg):
        msg['repost'] = True
        msg['src'] =  via[1]
    
    forv = getForvarded(m)
    if (forv[0]):
        msg['repost'] = True
        msg['src'] =  forv[1]
    
    content = getContent(m)
    if (content is not None):
        msg['content'] = content
    
    replyTo = getReplyTo(m)
    if replyTo[0]:
        msg['replyTo'] = replyTo[1]
    
    linkGroups = getLinks(m)
    if linkGroups:
        (links, calls, hashtags) = linkGroups
        if links:
            msg['links'] = links
        
        if calls:
            msg['calls'] = calls
        
        if hashtags:
            msg['hashtags'] = hashtags
        
    msgs.append(msg)


def getM(id):    
    return next(m for m in msgs if m['id']==id)

def getOrigin(id):
    return messages[msgs.index(getM(id))]


rest_dates_str = ["01.01", "02.01", "03.01", "04.01", "07.01", "08.01", "08.03", "01.05", "02.05", "03.05", "09.05", "10.05", "12.06", "04.11"] 
rest_dates = [datetime.datetime.strptime(d+".2019", "%d.%m.%Y").date() for d in rest_dates_str]

def isWorkDay(dt):
    def isWorkDay_special(dt):
        if dt in rest_dates:
            return False
        return None
    
    def isWorkDay_common(dt):
        return dt.isoweekday() in {1,2,3,4,5}
    
    sp = isWorkDay_special(dt)
    if (sp is not None):
        return sp
    else:
        return isWorkDay_common(dt)


def days2019():
    return (datetime.date(2019,1,1) + datetime.timedelta(n) for n in range(365))



